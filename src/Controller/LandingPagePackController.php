<?php

namespace App\Controller;

use App\Entity\LandingPageContent;
use App\Repository\LandingPageContentRepository;
use App\Repository\LandingPagePackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class LandingPagePackController extends AbstractController
{
    #[Route('/landing-page', name: 'app_landing_page_pack')]
    public function index(LandingPageContentRepository $landingPageContentRepository): JsonResponse
    {
        $data = $landingPageContentRepository->findAll();

        $response = array();

        foreach ($data as $page){
            $response[$page->getLandingPagePack()->getId()][] = ['title' => $page->getTitle(), 'text' => $page->getText()];

        }

        return $this->json($response);
    }

}
