<?php

namespace App\Controller;

use App\Entity\LandingPageContent;
use App\Entity\LandingPagePack;
use Doctrine\ORM\EntityManagerInterface;
use OpenAI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GenerateContentController extends AbstractController
{
    /**
     */
    #[Route('/generate', methods: ['POST'])]
    public function generate(Request $request, EntityManagerInterface $em): Response
    {
        $client = OpenAI::client($_ENV['GPT_TOKEN']);

        $result = $client->chat()->create([
            'model' => 'gpt-3.5-turbo',
            'messages' =>
                [
                    [
                        'role' => 'user',
                        'content' => "Give me a sentence similar to this '" . json_decode($request->getContent())->title . "'
                         and sentence similar to this '" . json_decode($request->getContent())->text . "', put the first sentence in title property and the second in content property 
                         in json format"
                    ]
                ],
            'temperature' => 0.5,
            'n' => 20
        ]);

        $landingPack = new LandingPagePack();
        $em->persist($landingPack);
        $em->flush();

        foreach ($result['choices'] as $choice) {
            $data = json_decode($choice['message']['content']);
            $landingPage = new LandingPageContent();
            $landingPage->setTitle($data->title);
            $landingPage->setText($data->content);
            $landingPage->setLandingPagePack($landingPack);
            $em->persist($landingPage);
            $em->flush();
        }

        return new Response(json_encode($result['choices']));
    }
}