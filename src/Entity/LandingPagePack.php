<?php

namespace App\Entity;

use App\Repository\LandingPagePackRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LandingPagePackRepository::class)]
class LandingPagePack
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'landingPagePack', targetEntity: LandingPageContent::class, orphanRemoval: true)]
    private Collection $data;

    public function __construct()
    {
        $this->data = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, LandingPageContent>
     */
    public function getData(): Collection
    {
        return $this->data;
    }

    public function addData(LandingPageContent $data): self
    {
        if (!$this->data->contains($data)) {
            $this->data->add($data);
            $data->setLandingPagePack($this);
        }

        return $this;
    }

    public function removeData(LandingPageContent $data): self
    {
        if ($this->data->removeElement($data)) {
            // set the owning side to null (unless already changed)
            if ($data->getLandingPagePack() === $this) {
                $data->setLandingPagePack(null);
            }
        }

        return $this;
    }
}
