import React, { useState } from "react";


function Form({setReloadData}) {
    const [title, setTitle] = useState('')
    const [text, setText] = useState('')
    const [pending, setPending] = useState(false)

    const handleSubmit = (e) => {
        setPending(true);
        const data = { title: title, text: text };
        fetch("https://localhost/generate", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log("Success:", data);
                setPending(false);
                setReloadData(true);
            })
            .catch((error) => {
                console.error("Error:", error);
                setPending(false);
            });
        e.preventDefault()
    }
    return (
        <div>
            {pending ? <h1>Generation in progress ...</h1> :
                <form onSubmit={e => { handleSubmit(e) }}>
                    <input
                        type="text"
                        name="title"
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                        placeholder="Title"
                        required
                    />
                    <input
                        type="text"
                        name="text"
                        value={text}
                        onChange={e => setText(e.target.value)}
                        placeholder="Text"
                        required
                    />
                    <button type="submit">Submit</button>
                </form>
            }
        </div>
    )
}

export default Form