import {useEffect, useState} from "react";
import Form from "./Form";

function List({reloadData, setReloadData}) {
    const [allPages, setAllPages] = useState([]);

    useEffect(() => {
        fetch('https://localhost/landing-page').then(
            (response) => response.json()
        ).then((data) => {
            let arr = []
            for (const pack in data) {
                arr.push(data[pack]);
            }
            setAllPages(arr)
            setReloadData(false)
        })
    },[reloadData])

    function renderLandingPage(elem) {
        const htmlContent = "<!DOCTYPE html>\n" +
            "<html lang='eng'>\n" +
            "<head>\n" +
            "    <title>Webyn: The best landing pages that convert your traffic</title>\n" +
            "    <description>Generate the best landing pages that convert your traffic using Generative AI and multi variate testing!</description>\n" +
            "</head>\n" +
            "<body>\n" +
            "<h1>" + elem.title + "</h1>\n" +
            "<p>" + elem.text + "</p>\n" +
            "</body>\n" +
            "</html>\n";

        const childUrl = window.open('', '_blank');
        childUrl.document.write(htmlContent);
        childUrl.document.close();
        return 'ok';
    }

    return (
        <div>
            <h1>Form to generate new set of landing page</h1>
            <Form setReloadData={setReloadData}/>
            <ul>
                {allPages.map((data, index) => {
                    return <li key={index}>Page pack number {index + 1}
                        {data.map((elem, index) => {
                            return <ul>
                                <li>
                                    <button onClick={() => renderLandingPage(elem)} key={index}>
                                        Test {index + 1}
                                    </button>
                                </li>
                            </ul>
                        })}
                    </li>
                })}
            </ul>
        </div>
    )
}

export default List;