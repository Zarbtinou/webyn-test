import "./App.css";
import List from "./components/List";
import {useState} from "react";

function App() {
    const [reloadData, setReloadData] = useState(false)

    return (
        <div className="App">
            <List setReloadData={setReloadData} reloadData={reloadData}/>
        </div>
    );
}

export default App;
