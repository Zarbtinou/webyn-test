<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230327135453 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE landing_page_content (id INT AUTO_INCREMENT NOT NULL, landing_page_pack_id INT NOT NULL, title VARCHAR(500) NOT NULL, text LONGTEXT NOT NULL, INDEX IDX_426FE236A5B36088 (landing_page_pack_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE landing_page_pack (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE landing_page_content ADD CONSTRAINT FK_426FE236A5B36088 FOREIGN KEY (landing_page_pack_id) REFERENCES landing_page_pack (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE landing_page_content DROP FOREIGN KEY FK_426FE236A5B36088');
        $this->addSql('DROP TABLE landing_page_content');
        $this->addSql('DROP TABLE landing_page_pack');
    }
}
